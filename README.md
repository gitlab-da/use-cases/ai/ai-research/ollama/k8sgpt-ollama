# K8sgpt with local Ollama

Use [ks8gpt](https://k8sgpt.ai/) with a local [Ollama](https://ollama.com/) instance and LLMs (Mistral, etc.).

![](docs/assets/images/k8sgpt_ollama_analyze_kubernetes_deployment_crashloopbackoff.png)

## Events / Talks

Example shown in @dnsmichi 's talks:

1. [QCon London 2024](https://qconlondon.com/presentation/apr2024/efficient-devsecops-workflows-little-help-ai) - [slides](https://go.gitlab.com/ZDYNXQ) - [Infoq.com article](https://www.infoq.com/news/2024/04/qcon-london-devsecops-ai/)
1. [Container Days EU 2024](https://www.containerdays.io/containerdays-conference-2024/speakers/) - [slides](https://go.gitlab.com/7SDsCE)
1. [InfoQ Dev Summit Munich 2024](https://devsummit.infoq.com/presentation/munich2024/efficient-devsecops-workflows-little-help-ai) - [slides](TODO)
1. [IT-Tage 2024](https://www.ittage.informatik-aktuell.de/programm/2024/effiziente-devsecops-workflows-with-a-little-help-from-ai.html) - [slides](TODO)

## Usage

### Installation

[Install Ollama](https://ollama.com/download) and pull the Mistral LLM.

```shell
ollama pull mistral
```

[Install k8sgpt](https://github.com/k8sgpt-ai/k8sgpt/blob/main/README.md#cli-installation), for example on macOS:

```shell
brew tap k8sgpt-ai/k8sgpt
brew install k8sgpt
```

### Configuration

Configure k8sgpt with a fake openAI backend, using the Ollama API which is compatible with OpenAI. As a model, use Mistral, a more lightweight model for local LLMs.

When asked for an OpenAI key, just press enter.

```shell
k8sgpt auth list 

k8sgpt auth add --backend openai --model mistral --baseurl http://localhost:11434/v1
```

### Demos

Start a Kubernetes cluster, and access it. Example with Rancher Desktop on macOS:

```shell
kubectl config get-contexts
kubectl config use-context rancher-desktop 
```

#### Bad app

Create a bad deployment, and let k8sgpt analyze it.

```shell
kubectl create deploy bad-app --image=not-exist 
```

```shell
k8sgpt analyze  --filter=Pod --namespace=default  --language English --explain --no-cache --interactive
```

![](docs/assets/images/k8sgpt_ollama_analyze_kubernetes_deployment_problem.png)

#### CrashLoopBackoff

```shell
kubectl apply -f kubernetes/crashloopbackoff.yml
```

![](docs/assets/images/k8sgpt_ollama_analyze_kubernetes_deployment_crashloopbackoff.png)

#### Interactive - follow up prompts

```markdown
Show the YAML output with fixes

Only show the YAML blocks that need fixes
```

![](docs/assets/images/k8sgpt_ollama_analyze_kubernetes_deployment_interactive_fixes.png)

### Cleanup

```shell
kubectl get deployments

kubectl delete deployment bad-app
kubectl delete deployment nginx-deployment

kubectl get deployments
```

## Research

### Requirements

1. k8sgpt requires an openAI-compatible interface.
    - It does not support Ollama, nor Anthropic yet.
1. A demo with a broken Kubernetes cluster.
1. Ollama and local LLMs

### Tests

The k8sgpt blog links to a tutorial with local LLMs. https://medium.com/@panpan0000/empower-kubernetes-with-k8sgpt-using-open-source-llm-1b3fa021abd6 

Ollama provides OpenAI compatibility since 2024-02-08: https://ollama.com/blog/openai-compatibility 


```shell
curl http://localhost:11434/v1/chat/completions \
    -H "Content-Type: application/json" \
    -d '{
        "model": "llama2",
        "messages": [
            {
                "role": "system",
                "content": "You are a helpful assistant."
            },
            {
                "role": "user",
                "content": "Hello!"
            }
        ]
    }'
```

### k8sgpt override

```shell
k8sgpt auth add --backend openai --model mistral --baseurl http://localhost:11434/v1

kubectl create deploy bad-app --image=not-exist 

k8sgpt analyze  --filter=Pod --namespace=default  --language English --explain --no-cache
```

Works :D

More resources in https://anaisurl.com/k8sgpt-full-tutorial/ 